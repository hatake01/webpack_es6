# webpackの環境構築

- [目標](#目標)
- [目次](#目次)

## 目標
**babelでjsの変換できる環境をsteb by stepで構築する**

## 目次
- [webpackのインストール](./doc/install.md)
- [webpackの設定](./doc/webpackconfig.md)
- [その他設定ファイルについて](./doc/config.md)