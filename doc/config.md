# その他設定ファイルについて

## ターゲットブラウザの設定(.browserslistrc)

```text:.browserslistrc
> 0.1% in JP
not ie <= 8
```

## Balelの設定

```js:.browserlist.js
module.exports = {
  // presetsの設定
  // 今回は'@babel/preset-env'しか設定してないが、他のpresetsを使いたい場合はここの配列に追記していく
  "presets": [
    [
      "@babel/preset-env",
      {
        "useBuiltIns": "usage",
        "modules": false,
      }
    ]
  ],
  // webpackでbabelを通そうとするときはnode_modulesを除外設定する
  // https://qiita.com/haribote/items/73ede4fcdf9c942f1c4f
  "ignore": [
    "node_modules"
  ],

  // 使うプラグインを設定する
  // 使いたいプラグインを配列に追記していく
  // プラグインによってはさらに設定をすることが出来る。
  // その際は各プラグインのドキュメントを参照
  "plugins": [
    "@babel/plugin-proposal-class-properties",

    // babel-plugin-module-resolverの設定
    // "alias"の設定では任意の名前でaliasが作れる
    // 今回は「@」がついているが別につけなくていい(普通のディレクトリと見分けるため今回はつけている)
    [
      "module-resolver",
      {
        "root": ["./src/js"],
        "alias": {
          "@lib": "./src/js/lib"
        }
      }
    ]
  ]
}
```

## PostCssの設定

```js:.postcssrc.js
// https://github.com/michael-ciniawsky/postcss-load-config

module.exports = ({ env }) => {
  return {
    'plugins': {
      'postcss-import': {},
      'postcss-custom-properties': {},
      'postcss-combine-duplicated-selectors': {},
      // to edit target browsers: use 'browserslist' field in package.json
      'cssnano': {
        preset: ['default', {
          discardEmpty: true,
          mergeRules: true,
          normalizeWhitespace: false,
          minifySelectors: true,
        }]
      },
      'autoprefixer': {}
    }
  };
}
```
