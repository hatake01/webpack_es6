#webpackの設定

## 共通ファイル

```js:webpack.common.js
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

// Is the current build a development build
const IS_DEV = process.env.NODE_ENV === 'dev';

const dirNode = 'node_modules';
const dirApp = path.join(__dirname, '../src/js');
// const dirAssets = path.join(__dirname, 'assets');

const appHtmlTitle = 'Webpack Boilerplate!!!';

/**
 * Webpack Configuration
 */
module.exports = {
  entry: {
    bundle: path.join(dirApp, 'index')
  },
  resolve: {
    extensions: ['.js', '.json'],
    modules: [dirNode, dirApp]
  },
  plugins: [
    new webpack.DefinePlugin({
      IS_DEV: IS_DEV
    }),

    new HtmlWebpackPlugin({
      template: path.join(__dirname, '../src/index.pug'),
      title: appHtmlTitle,
      minify: true
    })
  ],
  module: {
    rules: [
      // BABEL
      {
        test: /\.m?js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      // PUG
      {
        test: /\.pug$/,
        use: [
          {
            loader: 'pug-loader',
            options: {
              pretty: true
            }
          }
        ],
        exclude: /(node_modules)/
      },
    ]
  }
};
```

## 開発用設定

```js:webpack.config.dev.js
const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const webpackConfig = require('./webpack.common');

module.exports = merge(webpackConfig, {
  devtool: 'source-map',

  output: {
    pathinfo: true,
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    filename: '[name].js'
  },

  devServer: {
    host: '0.0.0.0',
    port: 1234,
    hot: true,
    overlay: true
  },

  plugins: [new webpack.HotModuleReplacementPlugin()],

  module: {
    rules: [
      // STYLES
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: { importLoaders: 1 }
          },
          {
            loader: 'postcss-loader'
          },
          {
            loader: 'sass-loader',
            options: {
              outputStyle: 'expanded'
            }
          }
        ]
      }
    ]
  }
});

```

## 本番配信用設定

```js:webpack.config.prod.js
const path = require('path');
const merge = require('webpack-merge');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
// const CleanWebpackPlugin = require('clean-webpack-plugin');
const webpackConfig = require('./webpack.common');

module.exports = merge(webpackConfig, {

  devtool: 'source-map',

  output: {
    path: path.join(__dirname, '../dist'),
    filename: '[name].js'
  },

  module: {
    rules: [
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
          },
          {
            loader: 'css-loader',
            options: { importLoaders: 2 }
          },
          {
            loader: 'postcss-loader'
          },
          {
            loader: 'sass-loader'
          }
        ]
      }
    ]
  },

  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "[name].css",
      chunkFilename: "[id].css"
    })
    // new CleanWebpackPlugin(['dist'])
  ]

});
```