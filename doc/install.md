# webpackの環境構築
- [目標](#目標)
- [webpackのインストール](#webpackのインストール)
  - [webpack本体](#webpack本体)
  - [webpackのプラグイン](#webpackのプラグイン)
  - [webpackのプラグインではないがあると便利なモジュール](#webpackのプラグインではないがあると便利なモジュール)
- [各種サイト構築に必要なモジュールやローダーのインストール](#各種サイト構築に必要なモジュールやローダーのインストール)
  - [CSSまわり(postcss使用)](#CSSまわり(postcss使用))
    - [style-loader css-loaderをインストール](#style-loader%20css-loaderをインストール)
    - [postcss-loaderをインストール](#postcss-loaderをインストール)
  - [Babelをインストール](#Babelをインストール)
  - [pugをインストール](#pugをインストール)

## 目標
babelでjsの変換できる環境をsteb by stepで構築する
### 前提
- node や npm はインストール済み
- `npm i`は`npm install`の省略形
- `npm i -D`は`npm install --save-dev`の省略形
- .gitignore[ここから](https://github.com/github/gitignore/blob/master/Node.gitignore)拝借


## webpackのインストール
### webpack本体
```console
$ npm i -D webpack webpack-cli webpack-dev-server
```
フロントエンド開発でお世話になる基本
### webpackのプラグイン
```console
$ npm i -D html-webpack-plugin webpack-merge uglifyjs-webpack-plugin
```
- `webpack-merge` : 分割されたwebpack.configのデータをひとつにあわせる
- `html-webpack-plugin` : htmlを出力する
- `uglifyjs-webpack-plugin` : jsの最適化をする
### webpackのプラグインではないがあると便利なモジュール
```console
$ npm i -D cross-env
```
- `cross-env` : コマンドライン環境変数をmac,win問わず使えるようにする
## 各種サイト構築に必要なモジュールやローダーのインストール
[loaderって何かについてはこちらを参考](https://qiita.com/terrierscript/items/0574ab1ef358fecb55b9#loader%E3%81%A8plugin%E3%81%AE%E9%81%95%E3%81%84)

### CSSまわり(postcss使用)
#### style-loader css-loaderをインストール
style-loader css-loaderの違いは何かについては[こちらを参照](https://qiita.com/shuntksh/items/bb5cbea40a343e2e791a#%E3%81%AA%E3%81%9Cstyle-loader%E3%81%A8css-loader%E3%81%8C%E3%81%82%E3%82%8B%E3%81%AE%E3%81%8B)
```console
$ npm i -D style-loader css-loader
```
#### postcss-loaderをインストール
```console
$ npm i -D postcss-loader autoprefixer postcss-import postcss-nested postcss-extend postcss-mixins
```
- `autoprefixer` : 自動的にprefixをつけてくれる
- `postcss-combine-duplicated-selectors` : 同じセレクタで指定されているものを1つにまとめてくれる

<br>以下はsassを入れない場合sass like構文を使いたいときに

- `postcss-import` : importの解決をしてくれる
- `postcss-nested` : sassライクなネスト記法に対応してくれる
- `postcss-extend` : extendを使えるようにする([詳しくはこちら](https://github.com/travco/postcss-extend))
- `postcss-mixins` : mixinsを使えるようにする([詳しくはこちら](https://github.com/postcss/postcss-mixins))
- `postcss-custom-properties` : CSS Custom Propertiesが使えるようになる([詳しくはこちら](https://github.com/postcss/postcss-custom-properties))
- `postcss-for` : for文が使えるようになる([詳しくはこちら](https://github.com/antyakushev/postcss-for))

#### postcss-loaderをインストール
```console
$ npm i -D node-sass sass-loader
```

### Babelをインストール
```console
$ npm i -D babel-loader @babel/cli @babel/core @babel/preset-env @babel/plugin-proposal-class-properties babel-plugin-module-resolver 
```
- `@babel/cli`, `@babel/core` : 基本機能なので絶対インストール
- `@babel/preset-env` : polyfillを指定したブラウザに合わせて自動的に最適化してくれる  
※ブラウザの標準で機能が実装されている場合はpolifillを適用させないようにしてくれる  
万能じゃないので必要なpolyfillをnpmで音さなかいけない場合がある
- `@babel/plugin-proposal-class-properties` : class構文の機能を拡張してくれる[詳細はこちら](https://babeljs.io/docs/en/babel-plugin-proposal-class-properties)
- `babel-plugin-module-resolver` : importする際のパスのalias(ショートカット)を使えるようになる

### pugをインストール
```console
$ npm i -D pug pug-loader
```
