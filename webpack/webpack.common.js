const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

// Is the current build a development build
const IS_DEV = process.env.NODE_ENV === 'dev';

const dirNode = 'node_modules';
const dirApp = path.join(__dirname, '../src/js');
// const dirAssets = path.join(__dirname, 'assets');

const appHtmlTitle = 'Webpack Boilerplate!!!';

/**
 * Webpack Configuration
 */
module.exports = {
  entry: {
    bundle: path.join(dirApp, 'index')
  },
  resolve: {
    extensions: ['.js', '.json'],
    modules: [dirNode, dirApp]
  },
  plugins: [
    new webpack.DefinePlugin({
      IS_DEV: IS_DEV
    }),

    new HtmlWebpackPlugin({
      template: path.join(__dirname, '../src/index.pug'),
      title: appHtmlTitle,
      minify: true
    })
  ],
  module: {
    rules: [
      // BABEL
      {
        test: /\.m?js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      // PUG
      {
        test: /\.pug$/,
        use: [
          {
            loader: 'pug-loader',
            options: {
              pretty: true
            }
          }
        ],
        exclude: /(node_modules)/
      },
    ]
  }
};
