// https://github.com/michael-ciniawsky/postcss-load-config

module.exports = ({ env }) => {
  return {
    'plugins': {
      'postcss-import': {},
      'postcss-custom-properties': {},
      'postcss-combine-duplicated-selectors': {},
      // to edit target browsers: use 'browserslist' field in package.json
      'cssnano': {
        preset: ['default', {
          discardEmpty: true,
          mergeRules: true,
          normalizeWhitespace: false,
          minifySelectors: true,
        }]
      },
      'autoprefixer': {}
    }
  };
}
