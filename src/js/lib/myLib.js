export const add = (...arg) => {
  return arg.reduce((prev, current)=> {
    return prev + current;
  }, 0);
};

export const helloText = arg => {
  const hello = 'hello';
  return `${hello} ${arg}`;
};

export const factorial = n => {
  let rtn = 1;
  while(n>0) {
    rtn *= n--;
  }
  return rtn;
}