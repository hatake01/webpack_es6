import 'es6-promise/auto';
import '../css/top.scss';
import { add, helloText, factorial } from './lib/myLib';

console.log('call index.js');

const obj1 = {
  name: 'mike',
  age: '10'
}
const obj2 = {
  sex: 'm',
  country: 'JP'
}

const factNum = 6;

const domLoadHandler = (e) => {
  const myObj = Object.assign({}, obj1, obj2);
  console.log('call domLoadHandler');
  console.log('e', e);
  console.log('myObj', myObj);
  const dom = document.getElementById('app');
  console.dir(dom)
  // dom.textContent = JSON.stringify(myObj);
  dom.insertAdjacentHTML('beforeend','<p>Object.assign</p>')
  dom.insertAdjacentHTML('beforeend', `<p>${JSON.stringify(myObj)}</p>`);
  dom.insertAdjacentHTML('beforeend','<p>こんばんは</p>')
  dom.insertAdjacentHTML('beforeend',`<p>階乗 ${factNum}! = ${factorial(factNum)}</p>`)
  dom.insertAdjacentHTML('beforeend',`<p>call add ${add(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)}</p>`)
  dom.insertAdjacentHTML('beforeend',`<p>call helloText ${helloText('World')}</p>`)
  for(let i=1; i<=5; i++) {
    dom.insertAdjacentHTML('beforeend',`<p class="b-${i}">クラス.b-${i}</p>`)
  }
  console.dir(dom.textContent);
}

window.addEventListener('DOMContentLoaded', domLoadHandler);